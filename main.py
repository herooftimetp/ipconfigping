import socket
import os
from pythonping import ping
import sys

count = 1
while count < 3000:
    vraag = input("Wil je 1. je IP adres weten of een 2. ping sturen? toets 1 of 2 ")
    if vraag == "1":
        hostname = socket.gethostname()
        ip_address = socket.gethostbyname(hostname)
        print("Hostname: ", hostname)
        print("IP adres: ", ip_address)
    if vraag == "2":
        adres = input("Welk adres wil je pingen? ")
        try:
            ping(adres, verbose=True)
            opslaan = input("Wil je de input opslaan? toets dan 1, zo niet toets iets anders")
            if opslaan == "1":
                ping(adres, out=os.path.expanduser('~/Documents/pingoutput.txt'))
            elif opslaan != "1":
                pass
        except:
                        print("Dit is geen geldig ip adres of geldige domeinnaam","Foutcode:",sys.exc_info()[0])
    if vraag == "q":
        exit()
    else:
        print("probeer het nog eens of toets q om het programma te sluiten")


